# utf8-debug

Removes unwanted tokens like `Ã€` `â€º` stemming from an earlier conversion from
latin-1 to utf-8 charset gone awry.

## Installation

1. Clone the repository
2. If you don't have `cargo` installed, follow the instructions on [rustup.rs](https://rustup.rs/).
3. `cd` into the local repository and type `cargo install --path .`
4. If everything went fine, you should be able to use the command `utf8-debug`
   on your command line

## Usage

Before using the application, make sure that the file is recognised as utf-8.
You can check this with the command `file -i <filename>`. You should see
something like `<filename>: <mime-type>; charset=utf-8`. If you get another
charset (e.g. `unknown-8bit`) do a reconversion to utf-8 first:

```sh
iconv -f latin1 -t utf-8 <filename> -o <outfile>
```

Afterwards apply `utf-debug` on the converted file:

```sh
utf8-debug <infile> <outfile>
```

## Attribution

The conversion table is based on the list on [i18nqa.com](http://i18nqa.com/debug/utf8-debug.html).